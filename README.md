# DivinAI Datasets

<img src="Images/divinai.png" alt="The DivinAI project" class="center">

## Description
**divinAI** (**Div**ersity **in** **A**rtificial **I**ntelligence) is an collaborative initiative leaded by the [HUMAINT](https://ai-watch.ec.europa.eu/humaint_en) team at the Joint Research Center (EC).

The goal of divinAI is to research and develop a set of **diversity indicators**, related to Artificial Intelligence developments, with special focus on gender balance, geographical representation and presence of academia vs companies.

The aforementioned indicators aim to be informative about the following aspects:

* *Visibility of minorities in AI*: this item will be measured considering the presence of minorities invited as keynotes in AI big events.
* *Minorities authorship in AI research contributions*: this item will be measured by considering the gender/geographic/business profile of authors in AI conference proceedings.
* *Minorities presence in AI organisation committees*:  this item will be measured by considering the gender/geographic/business balance in the committees of AI conferences  

The previous indicators will be combined in order to assign one general diversity indicator to each big AI event. This can be useful to compare how different AI conferences care about minorities, and this might lead in raising interest for improving this indicator.

This goal will be addressed through the development of a collaborative website in which everybody can contribute by adding the following data related to the most relevant AI conferences worldwide:

Details about keynotes speakers
Details about members of the organisation committee
Details about authors (proceedings)

This repository complements the [DivinAI project's webpage](https://ai-watch.ec.europa.eu/humaint/divinai_en) providing the raw data that were used to compute diversity indexes in the different outcomes we have analysed (AI conferences, journals and professional associations).

## Acknowledgment

If you use our data, please reference the following works:  

* Porcaro, L. et al. [Behind Recommender Systems: the Geography of the ACM RecSys Community](https://arxiv.org/abs/2309.03512) (2023)
* Hupont, I. et al. [Measuring and fostering diversity in Affective Computing research](https://www.computer.org/csdl/journal/ta/5555/01/10041991/1KEtdOmkvKw) (2023)
* Hupont, I. et al. [Monitoring Diversity of AI Conferences: Lessons Learnt and Future Challenges in the DivinAI Project](https://arxiv.org/abs/2203.01657) (2022)
* Hupont, I. et al. [How diverse is the ACII community? Analysing gender, geographical and business diversity of Affective Computing research](https://arxiv.org/abs/2109.07907) (2021)
* Freire, A. et al. [Measuring diversity of artificial intelligence conferences](chrome-extension://efaidnbmnnnibpcajpcglclefindmkaj/https://proceedings.mlr.press/v142/freire21a/freire21a.pdf) (2021)

